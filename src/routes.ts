import * as express from 'express';
import { getGradeById } from './controller/StudentController';

export const router = express.Router();

router.get('/getResult/:id', getGradeById);
