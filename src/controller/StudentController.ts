import { resultByStudentId } from "../services/StudentService";
import { Request, Response } from "express";

export const getGradeById = async (req: Request, res: Response): Promise<any> => {
  const params = req.params;

  try {
    const result = await resultByStudentId(Number(params.id));

    console.log('success', 'getGradeById method returning success');
    return res.status(200).json({
      message: 200,
      data: result
    })
  } catch (error) {
    console.log("aquiii ", error);
    throw new Error(error);
  }
}

