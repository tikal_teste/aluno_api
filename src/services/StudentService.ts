import { IGrades } from "src/interface/IGrades";
import { db } from "../config/database";

export const resultByStudentId = async (id: number) => {
  const findStudent = await db('alunos').where({ id });

  if(!findStudent.length) {
    throw new Error(`Student not found by id ${id}`);
  }

  const findGrade = await db('notas').where({ id_aluno: id });

  if(!findGrade.length) {
    throw new Error(`Grade not found by student ${findStudent}`);
  }

  const calculate = calculateStudentGrade(findGrade[0]);

  return {
    ...findStudent[0],
    notas: findGrade,
    situacao: calculate
  }
}

export const calculateStudentGrade = ( data: IGrades ) => {
  const calculateMedia = (data.n1 + data.n2 + data.n3 + data.n4) / 4;
  let status = null;

  if ( calculateMedia >= 6 ) {
    status = "aprovado";
  }

  if ( calculateMedia >= 4 && calculateMedia < 6) {
    status = "recuperacao"
  }

  if ( calculateMedia < 4) {
    status = "reprovado"
  }

  return {
    media: calculateMedia,
    status
  };
}
