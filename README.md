# Instalação

Instalação das dependências
- npm install

Rodando o Projeto
- npm run dev

# Rotas

- Visualiza a nota de um aluno específico e diz sua situação na escola ( aprovado, recuperação ou reprovado )
  .get('/getResult/:id')
