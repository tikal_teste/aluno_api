import type { Knex } from "knex";
import { config as configDotenv } from 'dotenv';

configDotenv();

const config: { [key: string]: Knex.Config } = {

  development: {
    client: "pg",
    connection: {
      host : process.env.DB_HOST || '127.0.0.1',
      user : process.env.DB_USER || 'root',
      password : process.env.DB_PASSWORD || '123123',
      database : process.env.DB_DATABASE || 'teste'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: "knex_migrations"
    }
  }

};

module.exports = config;
